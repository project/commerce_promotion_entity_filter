<?php

namespace Drupal\commerce_promotion_entity_filter\Plugin\Commerce\PromotionOffer;

use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\PromotionOfferInterface;
use Drupal\Core\Entity\Query\ConditionInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Interface for extending Commerce Promotion offer plugins for entity filters.
 */
interface PromotionOfferEntityQueryInterface extends PromotionOfferInterface {

  /**
   * Generates a query condition or condition group for the entity filter query.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The main entity query.
   *
   * @return \Drupal\Core\Entity\Query\ConditionInterface|null
   *   Should return a condition, condition group, or NULL if no conditions.
   */
  public function getQueryCondition(QueryInterface $query) : ConditionInterface|null;

}
