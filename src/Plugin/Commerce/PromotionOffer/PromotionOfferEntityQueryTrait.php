<?php

namespace Drupal\commerce_promotion_entity_filter\Plugin\Commerce\PromotionOffer;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Helper trait to make sure we're only working on purchasable entities.
 */
trait PromotionOfferEntityQueryTrait {

  /**
   * Determines if entity type id class implements PurchasableEntityInterface.
   *
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return bool
   *   Returns TRUE if entity class implements PurchasableEntityInterface.
   */
  public function entityImplementsPurchasableEntityInterface(string $entity_type_id) : bool {
    $definition = \Drupal::entityTypeManager()->getDefinition($entity_type_id);
    // The definition should exist.
    if (!$definition instanceof EntityTypeInterface) {
      return FALSE;
    }
    return array_key_exists(PurchasableEntityInterface::class, class_implements($definition->getClass()));
  }

}
