<?php

namespace Drupal\commerce_promotion_entity_filter\Plugin\Commerce\PromotionOffer;

use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\OrderFixedAmountOff as CoreOrderFixedAmountOff;
use Drupal\Core\Entity\Query\ConditionInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Override class to implement promotion offer entity queries.
 */
class OrderFixedAmountOff extends CoreOrderFixedAmountOff implements PromotionOfferEntityQueryInterface {

  use PromotionOfferEntityQueryTrait;

  /**
   * {@inheritdoc}
   */
  public function getQueryCondition(QueryInterface $query): ConditionInterface|null {
    // Assert the entity type implements PurchasableEntityInterface.
    assert($this->entityImplementsPurchasableEntityInterface($query->getEntityTypeId()));
    // No conditions, plugin supports entire order.
    return NULL;
  }

}
