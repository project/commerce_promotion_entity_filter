<?php

namespace Drupal\commerce_promotion_entity_filter\Plugin\Commerce\PromotionOffer;

use Drupal\commerce\ConditionManagerInterface;
use Drupal\commerce_order\PriceSplitterInterface;
use Drupal\commerce_price\Resolver\ChainPriceResolverInterface;
use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\BuyXGetY as CoreBuyXGetY;
use Drupal\commerce_promotion_entity_filter\Event\CommerceConditionEntityQueryEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\ConditionInterface;
use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Override class to implement entity filters against promotion offer plugins.
 */
class BuyXGetY extends CoreBuyXGetY implements PromotionOfferEntityQueryInterface {

  use PromotionOfferEntityQueryTrait;

  /**
   * Drupal Event Dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RounderInterface $rounder, PriceSplitterInterface $splitter, ConditionManagerInterface $condition_manager, ChainPriceResolverInterface $chain_price_resolver, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $rounder, $splitter, $condition_manager, $chain_price_resolver, $entity_type_manager);
    $this->eventDispatcher = \Drupal::service('event_dispatcher');
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryCondition(QueryInterface $query): ConditionInterface|null {
    // Assert the entity type implements PurchasableEntityInterface.
    assert($this->entityImplementsPurchasableEntityInterface($query->getEntityTypeId()));

    $conditions_group = $this->buildConditionGroup($this->getConfiguration()['buy_conditions']);
    // Buy X Get Y is always an OR group.
    $query_condition = $query->orConditionGroup();

    // Allow each Commerce Condition to modify the query condition.
    foreach ($conditions_group->getConditions() as $condition) {
      $event = new CommerceConditionEntityQueryEvent($query_condition, $condition);
      $this->eventDispatcher->dispatch($event, 'commerce_promotion_entity_filter.' . $query->getEntityTypeId() . '.' . $condition->getPluginId());
      $query_condition = $event->getQueryCondition();
    }

    // If any sub-conditions exist, return the main query condition group.
    if ($query_condition->conditions()) {
      return $query_condition;
    }

    // Fallback to no condition filters.
    return NULL;
  }

}
