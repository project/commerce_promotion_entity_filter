<?php

namespace Drupal\commerce_promotion_entity_filter\Plugin\Commerce\PromotionOffer;

use Drupal\commerce_price\RounderInterface;
use Drupal\commerce_promotion\Plugin\Commerce\PromotionOffer\OrderItemPercentageOff as CoreOrderItemPercentageOff;
use Drupal\commerce_promotion_entity_filter\Event\CommerceConditionEntityQueryEvent;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Entity\Query\ConditionInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Override class to implement promotion offer entity queries.
 */
class OrderItemPercentageOff extends CoreOrderItemPercentageOff implements PromotionOfferEntityQueryInterface {

  use PromotionOfferEntityQueryTrait;

  /**
   * Drupal Event Dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RounderInterface $rounder, ContainerAwareEventDispatcher $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $rounder);
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_price.rounder'),
      $container->get('event_dispatcher'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQueryCondition(QueryInterface $query): ConditionInterface|null {
    // Assert the entity type implements PurchasableEntityInterface.
    assert($this->entityImplementsPurchasableEntityInterface($query->getEntityTypeId()));

    // Build a condition group based on the plugin's operator.
    if ($this->getConditionOperator() === 'OR') {
      $query_condition = $query->orConditionGroup();
    }
    else {
      $query_condition = $query->andConditionGroup();
    }

    // Allow each Commerce Condition to modify the query condition.
    foreach ($this->getConditions() as $condition) {
      $event = new CommerceConditionEntityQueryEvent($query_condition, $condition);
      $this->eventDispatcher->dispatch($event, 'commerce_promotion_entity_filter.' . $query->getEntityTypeId() . '.' . $condition->getPluginId());
      $query_condition = $event->getQueryCondition();
    }

    // If any sub-conditions exist, return the main query condition group.
    if ($query_condition->conditions()) {
      return $query_condition;
    }

    // Fallback to no condition filters.
    return NULL;
  }

}
