<?php

namespace Drupal\commerce_promotion_entity_filter;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_promotion\Entity\PromotionInterface;
use Drupal\commerce_promotion_entity_filter\Event\CommerceConditionEntityQueryEvent;
use Drupal\commerce_promotion_entity_filter\Plugin\Commerce\PromotionOffer\PromotionOfferEntityQueryInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Service to find applicable entities on a given promotion.
 */
class ApplicableEntitiesManager {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructs an ApplicableEntities object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Gets the applicable purchasable entities for a given promotion.
   *
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *   The promotion entity.
   * @param string $entity_type_id
   *   The purchasable entity type id.
   * @param bool $include_promotion_conditions
   *   Determines whether to include the promotion entity query conditions.
   *
   * @return \Drupal\commerce\PurchasableEntityInterface[]
   *   Returns an array of purchasable entities applicable to the promotion.
   */
  public function getApplicableEntities(PromotionInterface $promotion, string $entity_type_id, bool $include_promotion_conditions = FALSE) : array {
    return $this->entityTypeManager->getStorage($entity_type_id)->loadMultiple($this->getApplicableEntityIds($promotion, $entity_type_id, $include_promotion_conditions));
  }

  /**
   * Gets the applicable purchasable entity ids for a given promotion.
   *
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *   The promotion entity.
   * @param string $entity_type_id
   *   The purchasable entity type id.
   * @param bool $include_promotion_conditions
   *   Determines whether to include the promotion entity query conditions.
   *
   * @return array
   *   Returns an array of purchasable entity ids applicable to the promotion.
   */
  public function getApplicableEntityIds(PromotionInterface $promotion, string $entity_type_id, bool $include_promotion_conditions = FALSE) : array {
    return $this->getApplicableEntitiesQuery($promotion, $entity_type_id, $include_promotion_conditions)->execute();
  }

  /**
   * Gets an executable query that retrieves applicable purchasable entities.
   *
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *   The promotion entity.
   * @param string $entity_type_id
   *   The purchasable entity type id.
   * @param bool $include_promotion_conditions
   *   Determines whether to include the promotion entity query conditions.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   Returns an executable query.
   */
  public function getApplicableEntitiesQuery(PromotionInterface $promotion, string $entity_type_id, bool $include_promotion_conditions = FALSE) : QueryInterface {
    $definition = $this->entityTypeManager->getDefinition($entity_type_id, FALSE);
    assert($definition instanceof EntityTypeInterface);
    assert(array_key_exists(PurchasableEntityInterface::class, class_implements($definition->getClass())));

    $offer = $promotion->getOffer();
    assert($offer instanceof PromotionOfferEntityQueryInterface);

    $query = $this->entityTypeManager->getStorage($entity_type_id)->getQuery();
    $query->accessCheck(TRUE);

    // If the entity supports the EntityPublishedTrait, only load published.
    if (array_key_exists(EntityPublishedTrait::class, $this->classUsesDeep($definition->getClass()))) {
      $query->condition($definition->getKey('published'), TRUE);
    }

    // Handle the offer conditions.
    if ($offer_condition = $offer->getQueryCondition($query)) {
      $query->condition($offer_condition);
    }

    // Stop here if we only need the offer results.
    if (!$include_promotion_conditions) {
      return $query;
    }

    // Handle the promotion conditions.
    if ($promotion->getConditionOperator() === "OR") {
      $query_condition = $query->orConditionGroup();
    }
    else {
      $query_condition = $query->andConditionGroup();
    }

    foreach ($promotion->getConditions() as $condition) {
      $event = new CommerceConditionEntityQueryEvent($query_condition, $condition);
      $this->eventDispatcher->dispatch($event, 'commerce_promotion_entity_filter.' . $query->getEntityTypeId() . '.' . $condition->getPluginId());
      $query_condition = $event->getQueryCondition();
    }

    if ($query_condition->conditions()) {
      $query->condition($query_condition);
    }

    return $query;

  }

  /**
   * Determines if an entity is applicable to a promotion entity.
   *
   * @param \Drupal\commerce_promotion\Entity\PromotionInterface $promotion
   *   The promotion entity.
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The purchasable entity to check.
   * @param bool $include_promotion_conditions
   *   Determines whether to include promotion entity conditions with offer.
   *
   * @return bool
   *   Returns TRUE if entity is applicable to promotion, FALSE otherwise.
   */
  public function isApplicable(PromotionInterface $promotion, PurchasableEntityInterface $entity, bool $include_promotion_conditions = FALSE) : bool {
    $query = $this->getApplicableEntitiesQuery($promotion, $entity->getEntityTypeId(), $include_promotion_conditions);

    $definition = $this->entityTypeManager->getDefinition($entity->getEntityTypeId());

    $query->condition($definition->getKey('id'), $entity->id());

    if ($query->execute()) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Gets all promotions applicable to the entity.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The purchasable entity.
   * @param bool $include_promotion_conditions
   *   Determines whether to include promotion entity conditions with offer.
   *
   * @return \Drupal\commerce_promotion\Entity\PromotionInterface[]
   *   Returns an array of promotion entities.
   */
  public function getPromotionsByPurchasableEntity(PurchasableEntityInterface $entity, bool $include_promotion_conditions = FALSE) : array {
    $results = [];
    /** @var \Drupal\commerce_promotion\Entity\PromotionInterface[] $promotions */
    $promotions = $this->entityTypeManager->getStorage('commerce_promotion')->loadMultiple();
    foreach ($promotions as $promotion) {
      if ($this->isApplicable($promotion, $entity, $include_promotion_conditions)) {
        $results[] = $promotion;
      }
    }

    return $results;
  }

  /**
   * Returns all of the traits used by a class recursively.
   *
   * @param string $class
   *   The class.
   * @param bool $autoload
   *   Use autoloader.
   *
   * @return array
   *   Returns a key / value array of traits uses.
   */
  private function classUsesDeep(string $class, bool $autoload = TRUE) : array {
    $traits = class_uses($class, $autoload);
    if ($parent = get_parent_class($class)) {
      $traits = array_merge($traits, $this->classUsesDeep($parent, $autoload));
    }
    foreach ($traits as $trait) {
      $traits = array_merge($traits, $this->classUsesDeep($trait, $autoload));
    }

    return $traits;
  }

}
