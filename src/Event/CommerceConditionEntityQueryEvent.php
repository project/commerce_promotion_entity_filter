<?php

namespace Drupal\commerce_promotion_entity_filter\Event;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface as CommerceConditionInterface;
use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\Query\ConditionInterface as QueryCondition;

/**
 * Event for handling applicable entity filters on promotions via conditions.
 */
class CommerceConditionEntityQueryEvent extends Event {

  /**
   * Database query condition.
   *
   * @var \Drupal\Core\Database\Query\Condition
   */
  protected $queryCondition;

  /**
   * Commerce condition.
   *
   * @var \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface
   *   The commerce condition.
   */
  protected $commerceCondition;

  /**
   * Builds the filter event.
   *
   * @param \Drupal\Core\Entity\Query\ConditionInterface $query_condition
   *   Entity Query Condition Group.
   * @param \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface $commerce_condition
   *   Commerce Condition plugin instance.
   */
  public function __construct(QueryCondition $query_condition, CommerceConditionInterface $commerce_condition) {
    $this->queryCondition = $query_condition;
    $this->commerceCondition = $commerce_condition;
  }

  /**
   * Gets the Entity Query Condition Group.
   *
   * @return \Drupal\Core\Entity\Query\ConditionInterface
   *   Returns the query condition group.
   */
  public function getQueryCondition() : QueryCondition {
    return $this->queryCondition;
  }

  /**
   * Gets the Commerce Condition.
   *
   * @return \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface
   *   Returns the Commerce Condition plugin instance.
   */
  public function getCommerceCondition() : CommerceConditionInterface {
    return $this->commerceCondition;
  }

}
