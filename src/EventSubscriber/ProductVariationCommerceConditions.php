<?php

namespace Drupal\commerce_promotion_entity_filter\EventSubscriber;

use Drupal\commerce_promotion_entity_filter\Event\CommerceConditionEntityQueryEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Subscribes to built-in Commerce Promotion Offer plugin filter events.
 */
class ProductVariationCommerceConditions implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // Promotion Offer Conditions.
      'commerce_promotion_entity_filter.commerce_product_variation.order_item_product' => ['product'],
      'commerce_promotion_entity_filter.commerce_product_variation.order_item_purchased_entity:commerce_product_variation' => ['productVariation'],
      'commerce_promotion_entity_filter.commerce_product_variation.order_item_product_type' => ['productType'],
      'commerce_promotion_entity_filter.commerce_product_variation.order_item_variation_type' => ['productVariationType'],
      // @todo add category filter here.
      // Promotion Entity Conditions.
      'commerce_promotion_entity_filter.commerce_product_variation.order_product' => ['product'],
      'commerce_promotion_entity_filter.commerce_product_variation.order_purchased_entity:commerce_product_variation' => ['productVariation'],
      'commerce_promotion_entity_filter.commerce_product_variation.order_product_type' => ['productType'],
      'commerce_promotion_entity_filter.commerce_product_variation.order_variation_type' => ['productVariationType'],
      // @todo add category filter here.
    ];
  }

  /**
   * Handles the order_item_product plugin.
   *
   * @param \Drupal\commerce_promotion_entity_filter\Event\CommerceConditionEntityQueryEvent $event
   *   The filter event.
   */
  public function product(CommerceConditionEntityQueryEvent $event) {
    /** @var \Drupal\commerce_product\Plugin\Commerce\Condition\OrderItemProduct $commerce_condition */
    $commerce_condition = $event->getCommerceCondition();
    foreach ($commerce_condition->getConfiguration()['products'] as $product) {
      $event->getQueryCondition()->condition('product_id.entity.uuid', $product['product']);
    }
  }

  /**
   * Handles the order_item_purchased_entity:commerce_product_variation plugin.
   *
   * @param \Drupal\commerce_promotion_entity_filter\Event\CommerceConditionEntityQueryEvent $event
   *   The filter event.
   */
  public function productVariation(CommerceConditionEntityQueryEvent $event) {
    foreach ($event->getCommerceCondition()->getConfiguration()['entities'] as $uuid) {
      $event->getQueryCondition()->condition('uuid', $uuid);
    }
  }

  /**
   * Handles the order_item_product_type plugin.
   *
   * @param \Drupal\commerce_promotion_entity_filter\Event\CommerceConditionEntityQueryEvent $event
   *   The filter event.
   */
  public function productType(CommerceConditionEntityQueryEvent $event) {
    foreach ($event->getCommerceCondition()->getConfiguration()['product_types'] as $product_type) {
      $event->getQueryCondition()->condition('product_id.entity.type', $product_type);
    }
  }

  /**
   * Handles the order_item_variation_type plugin.
   *
   * @param \Drupal\commerce_promotion_entity_filter\Event\CommerceConditionEntityQueryEvent $event
   *   The filter event.
   */
  public function productVariationType(CommerceConditionEntityQueryEvent $event) {
    foreach ($event->getCommerceCondition()->getConfiguration()['variation_types'] as $variation_type) {
      $event->getQueryCondition()->condition('type', $variation_type);
    }
  }

}
